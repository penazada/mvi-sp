def calculate_metrics(tp, fp, fn):
    precision = tp / (tp + fp) if (tp + fp) != 0 else 0
    recall = tp / (tp + fn) if (tp + fn) != 0 else 0
    f_score = 2 * precision * recall / (precision + recall) if (precision + recall) != 0 else 0

    return precision, recall, f_score


if __name__ == '__main__':
    tp = 45 + 85
    fp = 18 + 48
    fn = 4 + 20

    precision, recall, f_score = calculate_metrics(tp, fp, fn)

    print(f"Precision: {precision:.2f}")
    print(f"Recall: {recall:.2f}")
    print(f"F-score: {f_score:.2f}")
