import nltk
import re
import pandas as pd
import time
import os

from bs4 import BeautifulSoup
from demo import ner
from concurrent.futures import ThreadPoolExecutor


def get_sentences(html_code):
    text = BeautifulSoup(html_code, 'html.parser').get_text()  # Getting text from html_code
    text = re.sub('\.*\n+', '. ', text)  # Replacing chain of dots or \n for one dot only
    sentences = nltk.tokenize.sent_tokenize(text)  # Cut text to sentences
    return sentences


def get_result(html_code):
    sentences = get_sentences(html_code)
    with ThreadPoolExecutor(max_workers=os.cpu_count()) as executor:
        results = list(executor.map(ner, sentences))  # Process each sentence in parallel

    # Merge results from sentences
    skills = list(set(skill for result in results for skill in result["skill"]))
    knowledges = list(set(knowledge for result in results for knowledge in result["knowledge"]))
    return {"skill": skills, "knowledge": knowledges}


def process_item(html_code):
    result = get_result(html_code)

    # Passing all skills again to have a better contex
    skill_string = '\n\n'.join(result['skill'])
    skill_result = ner(skill_string)

    # Sorting skills and knowledge for clarity
    skill_result['skill'].sort()
    result['knowledge'].sort()
    result_final = {"skill": skill_result['skill'], "knowledge": result['knowledge']}
    return result_final


def create_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)


def save_result(file_path, result):
    with open(file_path, 'w') as file:
        file.write('Skills:\n')
        for i, string in enumerate(result['skill']):
            file.write(f'{i + 1}: {string}\n')

        file.write('\nKnowledge:\n')
        for i, string in enumerate(result['knowledge']):
            file.write(f'{i + 1}: {string}\n')


data_path = "data/marketing_sample_for_trulia_com-real_estate__20190901_20191031__30k_data.csv"  # Path to dataset
results_dir_path = './results'  # Directory path (to save results)
description_index = 25362  # 0 - 30001 only

if __name__ == '__main__':
    df = pd.read_csv(data_path)  # Read the dataset
    nltk.download('punkt')  # Download the Punkt tokenizer

    create_dir(results_dir_path)  # Create a dir to save results

    html_code = df.loc[description_index, "Job Description"]  # Getting the job description from dataset

    start_time = time.time()  # Measure processing speed (start)

    result = process_item(html_code)  # Extract skills and knowledge

    end_time = time.time()  # Measure processing speed (end)

    save_result(os.path.join(results_dir_path, f'{description_index}.txt'), result)  # Save the result

    print(f"Processing time: {end_time - start_time: .4f} seconds")  # Print processing time
