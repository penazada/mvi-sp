# Retrieving Skills from Job Descriptions

... with BERT embeddings.

The dataset can be found at [Kaggle](https://www.kaggle.com/datasets/promptcloud/indeed-job-posting-dataset).

You can run the `demo.py` file, that accepts the text and prints Skills and Knowledge (used for milestone).

Then you can run the `main.py` file after specifying path to the Kaggle dataset (change the `data_path` variable). It can process the job
postings from this dataset. (It basically contains the whole solution to the problem)

Project structure:

```
├── jobbert -> the main source files for python code
│   ├── labels -> manual annotations
│   ├── results_eval -> model results for the labels with evalution
│   ├── demo.py -> used for milestone
│   └── main.py -> the whole Job Description processing
├── latex -> latex source for report
├── milestone -> milestone report
├── README.md -> this file
├── report.pdf -> report for the semestral project
└── vskills_practice_test.png -> screenshot of completed test
```